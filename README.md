# Lomiri Music App

Lomiri Music App is the official music player app for
[Ubuntu Touch](https://ubuntu-touch.io/). We follow an open source model where
the code is available to anyone to branch and hack on.

&nbsp;

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/com.ubuntu.music)


## Ubuntu Touch

This app is a core app for the [Ubuntu Touch](https://ubuntu-touch.io/) mobile
OS developed by [UBports](https://ubports.com/).

### Contributing

When contributing to this repository, feel free to first discuss the change you
wish to make via issue, email, Matrix ( #ubcd:matrix.org ), or any other method
with the owners of this repository before making a change.  Please note we have
a [code of conduct](https://ubports.com/code-of-conduct), please follow it in
all your interactions with the project.

### Reporting Bugs and Requesting Features

Bugs and feature requests can be reported via our
[bug tracker](https://gitlab.com/ubports/development/apps/lomiri-music-app/-/issues).

### Translating

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
[Hosted Weblate service](https://hosted.weblate.org/projects/lomiri/lomiri-music-app)

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.

### Contributing code

#### Building with clickable

The easiest way to build this app is using clickable by running the command:

```
clickable
```

See [clickable documentation](https://clickable-ut.dev/en/dev/index.html) for details.

#### Building and running on Desktop

Building and running the Ubuntu Music App on desktop is quite simple with clickable:

```
clickable desktop
```

Debugging on the Desktop is recommended.

#### Code Style

We are trying to use a common code style throughout the code base to maintain
uniformity and improve code clarity. Listed below are the code styles guides
that will be followed based on the language used.

* [QML](http://qt-project.org/doc/qt-5/qml-codingconventions.html)
* [JS, C++](https://google.github.io/styleguide/cppguide.html)
* Python - Code should follow PEP8 and Flake regulations

Note: In the QML code convention, ignore the Javascript code section guidelines.
So the sections that should be taken into account in the QML conventions are QML
Object Declarations, Grouped Properties and Lists.

#### Prerequisites to approving a Merge Request (MR)

Over time, it has been found that insufficient testing by reviewers sometimes
leads to music app not buildable due to manifest errors, or translation pot
file not updated. As such, please follow the checklist below before
top-approving a MR.

**Checklist**

*   Did you perform an exploratory manual test run of your code change and any
    related functionality?

*   Was the copyright years updated if necessary?

The above checklist is more of a guideline to help music app stay buildable,
stable and up to date.

## Donating

If you love UBports and it's apps please consider dontating to the
[UBports Foundation](https://ubports.com/donate). Thanks in advance for your
generous donations.
