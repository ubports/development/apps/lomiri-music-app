/*
 * Copyright: 2024 UBports
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import "../../logic/playlists.js" as Playlists


Dialog {
    id: dialogMissingFiles
    objectName: "dialogMissingFiles"

    readonly property int listLimit: 5

    property string playlistName
    property var errorList: []

    signal remove

    title: i18n.tr('Missing files in "%1"').arg(playlistName)

    Label {
        text: i18n.tr("These track files were not found in their original locations. Please check if they were moved, renamed or deleted.")
        wrapMode: Text.WordWrap
        anchors {
            left: parent.left
            right: parent.right
        }
    }

    Label {
        text: dialogMissingFiles.errorList.slice(0, dialogMissingFiles.listLimit).join("<br><br>")
        wrapMode: Text.WordWrap
        anchors {
            left: parent.left
            right: parent.right
        }
    }

    Label {
        readonly property int excessCount: dialogMissingFiles.errorList.length - dialogMissingFiles.listLimit

        visible: excessCount > 0
        text: i18n.tr("And %1 more files...").arg(excessCount)
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        anchors {
            left: parent.left
            right: parent.right
        }
    }

    Button {
        objectName: "removeFromPlaylistButton"
        text: i18n.tr("Remove from playlist")
        color: styleMusic.dialog.confirmRemoveButtonColor
        onClicked: {
            dialogMissingFiles.remove()
            PopupUtils.close(dialogMissingFiles)
        }
    }

    Button {
        objectName: "ignoreErrorsButton"
        text: i18n.tr("Ignore")
        color: styleMusic.dialog.cancelButtonColor
        onClicked: PopupUtils.close(dialogMissingFiles)
    }
}
