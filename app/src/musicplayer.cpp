#include "musicplayer.h"
#include "config.h"
#include <QQmlContext>

MusicPlayer::MusicPlayer (int& argc, char** argv) : QApplication (argc, argv), pView (nullptr)
{
    setOrganizationName ("music.ubports");
    setApplicationName ("music.ubports");
}

MusicPlayer::~MusicPlayer ()
{
    if (this->pView)
    {
        delete this->pView;
    }
}

bool MusicPlayer::setup ()
{
    this->pView = new QQuickView ();
    this->pView->setColor (Qt::black);
    this->pView->setResizeMode (QQuickView::SizeRootObjectToView);
    this->pView->setTitle (tr ("Music Player"));
    this->pView->rootContext ()->setContextProperty ("mApplication", this);
    QUrl sSource (musicPlayerDirectory () + "/app/music-app.qml");
    this->pView->setSource (sSource);
    this->pView->show ();

    return true;
}
