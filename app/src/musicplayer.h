#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include <QApplication>
#include <QQuickView>

class MusicPlayer : public QApplication
{
    Q_OBJECT
public:
    MusicPlayer (int& argc, char** argv);
    virtual ~MusicPlayer ();
    bool setup();
private:
    QQuickView* pView;
};

#endif
