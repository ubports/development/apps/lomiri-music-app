/*
 * Copyright (C) 2012 Canonical, Ltd.
 *
 * Authors:
 *  Ugo Riboni <ugo.riboni@canonical.com>
 *  Michał Sawicz <michal.sawicz@canonical.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCoreApplication>
#include <QDir>
#include <QString>

static const QString SNAP = qgetenv ("SNAP");
static const QString APP_DIR = qgetenv ("APP_DIR");
static const QString LOCALE_DIR = QStringLiteral ("@CMAKE_INSTALL_FULL_LOCALEDIR@");
static const QString DATA_DIR = QStringLiteral ("@DATA_DIR@");

inline bool isRunningInstalled ()
{
    static bool installed = (QCoreApplication::applicationDirPath () ==
        QDir (("@CMAKE_INSTALL_FULL_BINDIR@")).canonicalPath ());
    return installed;
}

inline QString localeDirectory (void)
{
    if (!SNAP.isEmpty())
    {
        return QDir::cleanPath (SNAP + QStringLiteral ("/") + LOCALE_DIR);
    }
    else if (!APP_DIR.isEmpty())
    {
        return QDir::cleanPath (APP_DIR + QStringLiteral ("/") + LOCALE_DIR);
    }
    else if (isRunningInstalled())
    {
        return QDir::cleanPath (LOCALE_DIR);
    }
    else
    {
        return QDir::currentPath();
    }
}

inline QString musicPlayerDirectory ()
{
    if (!SNAP.isEmpty())
    {
        return QDir::cleanPath (SNAP + QStringLiteral ("/") + DATA_DIR);
    }
    else if (!APP_DIR.isEmpty())
    {
        return QDir::cleanPath (APP_DIR + QStringLiteral ("/") + DATA_DIR);
    }
    else if (isRunningInstalled ())
    {
        return QDir::cleanPath (DATA_DIR);
    }
    else
    {
        return QDir::currentPath();
    }
}
