Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lomiri-music-app
Source: https://gitlab.com/ubports/development/apps/lomiri-music-app

Files: app/components/BlurredBackground.qml
 app/components/BlurredHeader.qml
 app/components/CoverGrid.qml
 app/components/Delegates/ActionDelegate.qml
 app/components/Delegates/Card.qml
 app/components/Delegates/MusicListItem.qml
 app/components/Dialog/ContentHubErrorDialog.qml
 app/components/Dialog/ContentHubNotFoundDialog.qml
 app/components/Dialog/ContentHubWaitDialog.qml
 app/components/Dialog/EditPlaylistDialog.qml
 app/components/Dialog/NewPlaylistDialog.qml
 app/components/Dialog/RemovePlaylistDialog.qml
 app/components/Flickables/MultiSelectListView.qml
 app/components/Flickables/MusicGridView.qml
 app/components/Flickables/MusicListView.qml
 app/components/HeadState/SearchHeadState.qml
 app/components/Helpers/ContentHubHelper.qml
 app/components/Helpers/UriHandlerHelper.qml
 app/components/Helpers/UserMetricsHelper.qml
 app/components/LibraryListModel.qml
 app/components/ListItemActions/AddToPlaylist.qml
 app/components/ListItemActions/AddToQueue.qml
 app/components/ListItemActions/AddToQueueAndPlaylist.qml
 app/components/ListItemActions/Remove.qml
 app/components/ListItemReorderComponent.qml
 app/components/MusicPage.qml
 app/components/MusicToolbar.qml
 app/components/NowPlayingFullView.qml
 app/components/NowPlayingSidebar.qml
 app/components/NowPlayingToolbar.qml
 app/components/Player.qml
 app/components/PlaylistsEmptyState.qml
 app/components/Queue.qml
 app/components/Style.qml
 app/components/ViewButton/PlayAllButton.qml
 app/components/ViewButton/QueueAllButton.qml
 app/components/ViewButton/ShuffleButton.qml
 app/components/Walkthrough/Slide1.qml
 app/components/Walkthrough/Slide2.qml
 app/components/Walkthrough/Slide3.qml
 app/components/Walkthrough/Walkthrough.qml
 app/components/WorkerModelLoader.qml
 app/components/WorkerWaiter.qml
 app/logic/meta-database.js
 app/logic/playlists.js
 app/logic/stored-request.js
 app/logic/worker-library-loader.js
 app/ui/AddToPlaylist.qml
 app/ui/Albums.qml
 app/ui/ArtistView.qml
 app/ui/Artists.qml
 app/ui/ContentHubExport.qml
 app/ui/Genres.qml
 app/ui/LibraryEmptyState.qml
 app/ui/NowPlaying.qml
 app/ui/Playlists.qml
 app/ui/Recent.qml
 app/ui/Songs.qml
 app/ui/SongsView.qml
Copyright: 2013, 2016, Andrew Hayzen <ahayzen@gmail.com>
  2013-2014, Andrew Hayzen <ahayzen@gmail.com>
  2013-2015, Andrew Hayzen <ahayzen@gmail.com>
  2013-2016, Andrew Hayzen <ahayzen@gmail.com>
  2014-2015, Andrew Hayzen <ahayzen@gmail.com>
  2014-2016, Andrew Hayzen <ahayzen@gmail.com>
  2015, Andrew Hayzen <ahayzen@gmail.com>
  2015-2016, Andrew Hayzen <ahayzen@gmail.com>
License: GPL-3
Comment:
 Other authors are listed in upstream's d/copyright file (but
 not listed in file headers):
    2013, Daniel Holm <d.holmen@gmail.com>
    2013, Daniel Kessel <d.kessel@gmx.de>
    2013, Lucas Romero Di Benedetto <lucasromerodb@gmail.com>
    2013, Victor Thompson <victor.thompson@gmail.com>

Files: .gitignore
 .gitlab-ci.yml
 AUTHORS
 CMakeLists.txt
 ChangeLog
 NEWS
 README.md
 app/CMakeLists.txt
 app/components/CMakeLists.txt
 app/components/Delegates/CMakeLists.txt
 app/components/Dialog/CMakeLists.txt
 app/components/Flickables/CMakeLists.txt
 app/components/HeadState/CMakeLists.txt
 app/components/Helpers/CMakeLists.txt
 app/components/ListItemActions/CMakeLists.txt
 app/components/Themes/artwork/bubble_arrow@20.png
 app/components/Themes/artwork/bubble_arrow@30.png
 app/components/Themes/artwork/bubble_arrow@8.png
 app/components/ViewButton/CMakeLists.txt
 app/components/Walkthrough/CMakeLists.txt
 app/graphics/CMakeLists.txt
 app/graphics/Ellipse@27.png
 app/graphics/Ellipse_15_opacity@27.png
 app/graphics/div@27.png
 app/graphics/music-app-splash.svg
 app/graphics/music-app.svg
 app/graphics/music_download_icon@27.png
 app/graphics/sd_phone_icon@27.png
 app/logic/CMakeLists.txt
 app/src/CMakeLists.txt
 app/src/main.cpp
 app/src/musicplayer.cpp
 app/src/musicplayer.h
 app/ui/CMakeLists.txt
 apparmor.json
 clickable.yaml
 lomiri-music-app.desktop.in.in
 lomiri-music-app-migrate.py
 manifest.json.in
 music-app-content.json
 music-app.qmlproject
 music-app.qmltheme
 lomiri-music-app.url-dispatcher
 po/CMakeLists.txt
 po/LINGUAS
 po/POTFILES.in
 tests/CMakeLists.txt
 tests/autopilot/CMakeLists.txt
 tests/autopilot/lomiri_music_app/CMakeLists.txt
 tests/autopilot/lomiri_music_app/content/blank-mediascanner-2.0/mediastore.db
 tests/autopilot/lomiri_music_app/content/blank-mediascanner-2.0/mediastore.sql
 tests/autopilot/lomiri_music_app/content/mediascanner-2.0/mediastore.db
 tests/autopilot/lomiri_music_app/content/mediascanner-2.0/mediastore.sch
 tests/autopilot/lomiri_music_app/content/mediascanner-2.0/mediastore.sql
 tests/autopilot/lomiri_music_app/tests/CMakeLists.txt
 tests/autopilot/lomiri_music_app/content/mediascanner-2.0/songs/1.ogg
 tests/autopilot/lomiri_music_app/content/mediascanner-2.0/songs/2.ogg
 tests/autopilot/lomiri_music_app/content/mediascanner-2.0/songs/3.mp3
 tests/manual/2015.music.ubports:music-tests/jobs/music-external.pxu
 tests/manual/2015.music.ubports:music-tests/jobs/music-library.pxu
 tests/manual/2015.music.ubports:music-tests/jobs/music-playlists.pxu
 tests/manual/2015.music.ubports:music-tests/jobs/music-queue.pxu
 tests/manual/2015.music.ubports:music-tests/jobs/music-recent.pxu
 tests/manual/2015.music.ubports:music-tests/manage.py
 tests/manual/2015.music.ubports:music-tests/whitelists/music-external.whitelist
 tests/manual/2015.music.ubports:music-tests/whitelists/music-library.whitelist
 tests/manual/2015.music.ubports:music-tests/whitelists/music-playlists.whitelist
 tests/manual/2015.music.ubports:music-tests/whitelists/music-queue.whitelist
 tests/manual/2015.music.ubports:music-tests/whitelists/music-recent.whitelist
Copyright: 2013-2016, Andrew Hayzen <ahayzen@gmail.com>
License: GPL-3
Comment:
 Assuming same copyright holderships and license as documented above.
 Also take notice of the additional copyright attributions in above
 paragraph's comment.

Files: po/*.po
 po/lomiri-music-app.pot
Copyright: 2013, Rosetta Contributors and Canonical Ltd.
  2014, Rosetta Contributors and Canonical Ltd.
  2015, Rosetta Contributors and Canonical Ltd.
  2016, Rosetta Contributors and Canonical Ltd.
  2013-2016, Canonical Ltd.
License: GPL-3
Comment:
 Assuming same license as used for the code files.

Files: app/components/HeadState/EmptyHeadState.qml
 app/components/HeadState/MultiSelectHeadState.qml
 app/components/HeadState/PlaylistHeadState.qml
 app/components/HeadState/PlaylistsHeadState.qml
 app/components/HeadState/QueueHeadState.qml
 app/components/HeadState/SearchableHeadState.qml
 app/music-app.qml
Copyright: 2013-2016, Andrew Hayzen <ahayzen@gmail.com>
  2015, Andrew Hayzen <ahayzen@gmail.com>
  2015-2016, Andrew Hayzen <ahayzen@gmail.com>
  2016, Andrew Hayzen <ahayzen@gmail.com>
  2020, UBports Foundation
License: GPL-3

Files: config.h.in
 app/components/LoadingSpinnerComponent.qml
 tests/autopilot/lomiri_music_app/__init__.py
 tests/autopilot/lomiri_music_app/tests/__init__.py
 tests/autopilot/lomiri_music_app/tests/test_music.py
Copyright: 2012, Canonical Ltd.
  2013-2016, Canonical Ltd.
  2013, 2015, Canonical Ltd.
  2013-2014, Canonical Ltd.
License: GPL-3

Files: app/components/Themes/Ambiance/BubbleShape.qml
 app/components/Themes/Ambiance/SliderStyle.qml
Copyright: 2016, Canonical Ltd.
License: LGPL-3

Files: app/components/Walkthrough/FirstRunWalkthrough.qml
Copyright: 2014-2015, Nekhelesh Ramananthan <nik90@ubuntu.com>
License: GPL-3

Files: app/ui/SettingsPage.qml
 app/components/Dialog/MissingFilesDialog.qml
Copyright: 2020, UBports Foundation
  2024, UBports Foundation
License: GPL-3

Files: debian/*
Copyright: 2013 Michael Hall <mhall119@ubuntu.com>
  2023-2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, strictly version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version 3 as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the LGPL v3 can be found in
 `/usr/share/common-licenses/LGPL-3'
